%IAGA
function best=ga
clear
MAX_gen=200;            %最大迭代步数
best.max_f=0;           %当前最大的适应度
STOP_f=14.5;            %停止循环的适应度
RANGE=[0 255];          %初始取值范围[0 255]
SPEEDUP_INTER=5;       %进入加速迭代的间隔
advance_k=0;            %优化的次数

popus=init;             %初始化
for gen=1:MAX_gen
    fitness=fit(popus,RANGE);       %求适应度
    f=fitness.f;
    picked=choose(popus,fitness);   %选择
    popus=intercross(popus,picked); %杂交
    popus=aberrance(popus,picked); %变异
    if max(f)>best.max_f
        advance_k=advance_k+1;
        x_better(advance_k)=fitness.x;
        best.max_f=max(f);
        best.popus=popus;
        best.x=fitness.x;
    end
    if mod(advance_k,SPEEDUP_INTER)==0
        RANGE=minmax(x_better);
        
        RANGE
        
        advance=0;
    end 
end
return;


function a=init
M=11;
a=round(rand(M,M));
for i=1:M
    a(i,i)=0;
end
for j=8:11
    a(1,i)=0;
end
if c(i,j)==0
    a(i,j)=0;
end
p=0;
for i=2:7
    e(i-1,1)=a(1,i)
end
for i=2:7
    p=p+a(1,i);
end
[max,I]=max(e);
if p>N %N为难民数
    e(I)=e(I)-p+N;
end
[min,I]=min(e);
if p<N
    e(I)=e(I)+N-p;
end
for i=2:7
    a(1,i)=e(i-1,1);
end
return;



function fitness=fit(popus,RANGE)%求适应度
[M,N]=size(popus);
fitness=0;
for m=1:M
    for n=1:N
        fitness=fitness+a(m,n)*d(m,n);
    end
end
return;

function picked=choose(popus,fitness)%选择
f=fitness.f;f_std=fitness.f_std;
[M,N]=size(popus);
choose_N=3;                 %选择choose_N对双亲
picked=zeros(choose_N,2);   %记录选择好的双亲
p=zeros(M,1);               %选择概率
d_order=zeros(M,1);

%把父代个体按适应度从大到小排序
f_t=sort(f,'descend');%将适应度按降序排列
for k=1:M
    x=find(f==f_t(k));%降序排列的个体序号
    d_order(k)=x(1);
end
for m=1:M
    popus_t(m,:)=popus(d_order(m),:);
end
popus=popus_t;
f=f_t;

p=f_std./sum(f_std);                    %选择概率
c_p=cumsum(p)';                          %累积概率

for cn=1:choose_N
    picked(cn,1)=roulette(c_p); %轮盘赌
    picked(cn,2)=roulette(c_p); %轮盘赌
    popus=intercross(popus,picked(cn,:));%杂交
end
popus=aberrance(popus,picked);%变异
return;

function popus=intercross(popus,picked) %杂交
[M_p,N_p]=size(picked);
[M,N]=size(popus);
for cn=1:M_p
    p(1)=ceil(rand*N);%生成杂交位置
    p(2)=ceil(rand*N);
    p=sort(p);
    t=popus(picked(cn,1),p(1):p(2));
    popus(picked(cn,1),p(1):p(2))=popus(picked(cn,2),p(1):p(2));
    popus(picked(cn,2),p(1):p(2))=t;
end
return;
function popus=aberrance(popus,picked) %变异
P_a=0.05;%变异概率
[M,N]=size(popus);
[M_p,N_p]=size(picked);
U=rand(1,2);

for kp=1:M_p
    if U(2)>=P_a        %如果大于变异概率，就不变异
        continue;
    end
    if U(1)>=0.5
        a=picked(kp,1);
    else
        a=picked(kp,2);
    end
    p(1)=ceil(rand*N);%生成变异位置
    p(2)=ceil(rand*N);
    if popus(a,p(1))==1%0 1变换
        popus(a,p(1))=0;
    else
        popus(a,p(1))=1;
    end
    if popus(a,p(2))==1
        popus(a,p(2))=0;
    else
        popus(a,p(2))=1;
    end
end
return;

function picked=roulette(c_p) %轮盘赌
[M,N]=size(c_p);
M=max([M N]);
U=rand;
if U<c_p(1)
    picked=1;
    return;
end
for m=1:(M-1)
    if U>c_p(m) & U<c_p(m+1)
        picked=m+1;
        break;
    end
end
