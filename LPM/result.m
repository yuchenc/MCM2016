function [flowA,fval,exitflag] = result(quotaB,distantD,m,data) 
R2=zeros(m,m*m);
for a=2:m
    for b=1:m
        R2(a,a+(b-1)*m)=R2(a,a+(b-1)*m)+1;
        R2(a,(a-1)*m+b)=R2(a,(a-1)*m+b)-1;
    end
end
E2 = quotaB;
R3=zeros(7,m*m);
for i=1:7
    R3(i,i)=1;
end
E3=zeros(7,1);
for i=1:7
    E3(i,1)=data(1,i);
end
A=zeros(1,m*m);
for i=1:m
    for j=1:m
        A(1,(i-1)*m+j)=distantD(i,j);
    end
end
R=[R2;R3];
E=[E2;E3];
LB=zeros(m*m,1);
UB=ones(m*m,1)*1400000;
[flowA,fval,exitflag]=linprog(A,[],[],R,E,LB,UB);


