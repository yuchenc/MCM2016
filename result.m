function [flowA,fval,exitflag] = result(quotaB,distantD,N) %组合优化函数%
m=11;%国家数%
%难民总数约束%
R1=zeros(1,m*m);
for a=1:m
    R1(1,a)=1;
end
E1 = N;
%配额约束%
R2=zeros(m,m*m);
for a=1:m
    for b=1:m
        R2(a,b*m)=R2(a,b*m)+1;
        R2(a,b)=R2(a,b)-1;
    end
end
E2 = quotaB; %quotaB为m*1%

%distantD矩阵转换%
A=zeros(1,m*m);
for i=1:m
    for j=1:m
        A(1,i*j)=distantD(i,j);
    end
end

UR=[];
UE=[];%无不等式约束%
R=[R1;R2];
E=[E1;E2];

[flowA,fval,exitflag]=linprog(A,UR,UE,R,E);


