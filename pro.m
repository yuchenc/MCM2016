fib = fopen('Weight.txt','r');
n=2;%A
a=[];
for i=1:n;
    tmp = str2num(fgets(fib));
    a=[a;tmp];
end
ri=[0 0 0.52 0.89 1.12 1.26 1.36 1.41 1.46 1.49 1.52 1.52 1.56 1.58 1.59];
[V,D]=eig(a);
lamda=max(diag(D));
num=find(diag(D)==lamda);
A=V(:,num)/sum(V(:,num));
cr=(lamda-n)/(n-1)/ri(n);
if cr>0.1
    fprintf('Matrix A is not consistent')
    return
end

n=3;%B1
a=[];
for i=1:n;
    tmp = str2num(fgets(fib));
    a=[a;tmp];
end
ri=[0 0 0.52 0.89 1.12 1.26 1.36 1.41 1.46 1.49 1.52 1.52 1.56 1.58 1.59];
[V,D]=eig(a);
lamda=max(diag(D));
num=find(diag(D)==lamda);
B1=V(:,num)/sum(V(:,num));
cr=(lamda-n)/(n-1)/ri(n);
if cr>0.1
    fprintf('Matrix B1 is not consistent')
    return
end

n=3;%B2
a=[];
for i=1:n;
    tmp = str2num(fgets(fib));
    a=[a;tmp];
end
ri=[0 0 0.52 0.89 1.12 1.26 1.36 1.41 1.46 1.49 1.52 1.52 1.56 1.58 1.59];
[V,D]=eig(a);
lamda=max(diag(D));
num=find(diag(D)==lamda);
B2=V(:,num)/sum(V(:,num));
cr=(lamda-n)/(n-1)/ri(n);
if cr>0.1
    fprintf('Matrix B2 is not consistent')
    return
end

n=4;%C11
a=[];
for i=1:n;
    tmp = str2num(fgets(fib));
    a=[a;tmp];
end
ri=[0 0 0.52 0.89 1.12 1.26 1.36 1.41 1.46 1.49 1.52 1.52 1.56 1.58 1.59];
[V,D]=eig(a);
lamda=max(diag(D));
num=find(diag(D)==lamda);
C11=V(:,num)/sum(V(:,num));
cr=(lamda-n)/(n-1)/ri(n);
if cr>0.1
    fprintf('Matrix C11 is not consistent')
    return
end

n=4;%C12
a=[];
for i=1:n;
     tmp = str2num(fgets(fib));
     a=[a;tmp];
end
ri=[0 0 0.52 0.89 1.12 1.26 1.36 1.41 1.46 1.49 1.52 1.52 1.56 1.58 1.59];
[V,D]=eig(a);
lamda=max(diag(D));
num=find(diag(D)==lamda);
C12=V(:,num)/sum(V(:,num));
cr=(lamda-n)/(n-1)/ri(n);
if cr>0.1
    fprintf('Matrix C12 is not consistent')
    return
end

n=6;%C13
a=[];
for i=1:n;
    tmp = str2num(fgets(fib));
    a=[a;tmp];
end
ri=[0 0 0.52 0.89 1.12 1.26 1.36 1.41 1.46 1.49 1.52 1.52 1.56 1.58 1.59];
[V,D]=eig(a);
lamda=max(diag(D));
num=find(diag(D)==lamda);
C13=V(:,num)/sum(V(:,num));
cr=(lamda-n)/(n-1)/ri(n);
if cr>0.1
    fprintf('Matrix C13 is not consistent')
    return
end

n=5;%C21
a=[];
for i=1:n;
    tmp = str2num(fgets(fib));
    a=[a;tmp];
end
ri=[0 0 0.52 0.89 1.12 1.26 1.36 1.41 1.46 1.49 1.52 1.52 1.56 1.58 1.59];
[V,D]=eig(a);
lamda=max(diag(D));
num=find(diag(D)==lamda);
C21=V(:,num)/sum(V(:,num));
cr=(lamda-n)/(n-1)/ri(n);
if cr>0.1
    fprintf('Matrix C21 is not consistent')
    return
end

n=4;%C22
a=[];
for i=1:n;
    tmp = str2num(fgets(fib));
    a=[a;tmp];
end
ri=[0 0 0.52 0.89 1.12 1.26 1.36 1.41 1.46 1.49 1.52 1.52 1.56 1.58 1.59];
[V,D]=eig(a);
lamda=max(diag(D));
num=find(diag(D)==lamda);
C22=V(:,num)/sum(V(:,num));
cr=(lamda-n)/(n-1)/ri(n);
if cr>0.1
    fprintf('Matrix C22 is not consistent')
    return
end

n=3;%C23
a=[];
for i=1:n;
    tmp = str2num(fgets(fib));
    a=[a;tmp];
end
ri=[0 0 0.52 0.89 1.12 1.26 1.36 1.41 1.46 1.49 1.52 1.52 1.56 1.58 1.59];
[V,D]=eig(a);
lamda=max(diag(D));
num=find(diag(D)==lamda);
C23=V(:,num)/sum(V(:,num));
cr=(lamda-n)/(n-1)/ri(n);
if cr>0.1
    fprintf('Matrix C23 is not consistent\n')
    return
end

W=[];
Q=[3 3];
for i=1:2;
    for j=1:Q(i);
        eval(char(['W=[W;A(i)*B',int2str(i),'(j)*C',int2str(i),int2str(j),'];']));
    end
end
fprintf('The weight of each variable:')
W

R=[];
Q=[3 3];
for i=1:2;
    eval(char(['R=[R;A(i)*B',int2str(i),'];']));
end
fprintf('The weight of each indicator:')
R
x=[0:pi/3:2*pi];
y=[R' R(1)];
polar(x,y,'r')