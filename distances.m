function [D]=distances(a) %a is the matrix of position 
[m,n] = size(a);
if n == 2
for i=1:m
    for j=1:m
        if j == i 
           D(i,j) = 0;
        else 
           D(i,j) = dis(a(i,:),a(j,:)); 
        end
    end
end
end
end

function [d]=dis(x,y) %x:longtitude y:latitude
x = x.* pi / 180;
y = y.* pi / 180;
R = 6371;
d = R * acos((sin(x(2)) * sin(y(2)) + cos(x(2)) * cos(y(2)) * cos(x(1) - y(1))));
end