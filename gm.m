function f=gm(x0,m)              %定义为函数gm(x)
n=length(x0);
x1=zeros(1,n);
x1(1)=x0(1);
for i=2:n                         %计算累加序列x1
    x1(i)=x1(i-1)+x0(i);
end
i=2:n;                            %对原始数列平行移位并负值给y
y(i-1)=x0(i);
y=y';
i=1:n-1;
c(i)=-0.5*(x1(i)+x1(i+1));
B=[c' ones(n-1,1)];
au=inv(B'*B)*B'*y;                  %计算参数a,u矩阵
i=1:m;                             %计算预测累加数列的值
ago(i)=(x0(1)-au(2)/au(1))*exp(-au(1)*(i-1))+au(2)/au(1);
yc(1)=ago(1);
i=1:m-1;                             %还原数列的值
yc(i+1)=ago(i+1)-ago(i);
i=2:n;
error(i)=yc(i)-x0(i);                     %计算残差值
yc(1)=ago(1);
i=1:m-1;                              %修正还原数列的值
yc(i+1)=ago(i+1)-ago(i);
c=std(error)/std(x0);                      %计算后验差比
relerror=abs((error)-mean(error)*ones(size(error)));
[nrow,ncol]=size(relerror);
p=0;
for i=2:ncol
    if  relerror(1,i)<0.6745*std(x0)
        p=p+1;
    end
end
p=p/(n-1);                     
w1=min(abs(error));
w2=max(abs(error));
i=1:n;                                     %计算关联度
w(i)=(w1+0.5*w2)./(abs(error(i))+0.5*w2);
w=sum(w)/(n-1);
au                                         %输出参数a,u的值
ago;                                        %输出累加数列ago的值
x0;                                         %输出原始序列值
f=yc;                                        %输出预测的值
error;                                        %输出残差的值
c;                                           %输出后验差比的值
p;                                           %输出小误差概率的值
w                                           %输出关联度
end                 